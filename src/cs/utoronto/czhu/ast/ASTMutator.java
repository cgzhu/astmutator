package cs.utoronto.czhu.ast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.jdt.core.*;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.core.runtime.*;

public class ASTMutator {
	public static void main(String[] args)
	{
		File file = new File("C:\\Users\\Polaris\\Desktop\\Management.java");
		try 
		{
			Map<String, Integer> map = new TreeMap<String, Integer>();
			
			FileInputStream fin = new FileInputStream(file);
			byte[] bytes = new byte[fin.available()];
			fin.read(bytes);
			String fileContent = new String(bytes);
			char[] sourceCode = fileContent.toCharArray();
			
			// creation of a Document
			Document document= new Document(fileContent);

			// creation of DOM/AST from a ICompilationUnit
			ASTParser parser = ASTParser.newParser(AST.JLS8);
			parser.setSource(sourceCode);
			ASTNode root = parser.createAST(null);
			CompilationUnit cu = (CompilationUnit)root;
			
			//ASTVisitor
			ASTVisitor visitor = new TraverseASTVisitor(map, cu, fileContent);
			root.accept(visitor);

			// creation of ASTRewrite
			ASTRewrite rewrite = ASTRewrite.create(cu.getAST());

			// description of the change
			//SimpleName oldName = ((TypeDeclaration)cu.types().get(0)).getName();
			//SimpleName newName = cu.getAST().newSimpleName("Y");
			
			//rewrite.replace(oldName, newName, null);
			
			
			
			
			
			
			
			
		    // computation of the text edits
			TextEdit edits = rewrite.rewriteAST(document, null);

			// computation of the new source code
			edits.apply(document);
			String newSource = document.get();

			//System.out.println(newSource);
			
			fin.close();
		} catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedTreeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
