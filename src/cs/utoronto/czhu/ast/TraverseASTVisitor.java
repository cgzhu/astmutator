package cs.utoronto.czhu.ast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class TraverseASTVisitor extends ASTVisitor{
	
	private Map<String, Integer> map;
	private CompilationUnit cu;
	private String fileContent;
	
	public TraverseASTVisitor(Map<String, Integer> map, CompilationUnit cu, String fileContent) {
		this.map = map;
		this.cu = cu;
		this.fileContent = fileContent;
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		// TODO Auto-generated method stub
		return super.visit(node);
	}

	@Override
	public boolean visit(MethodDeclaration node) {
		// TODO Auto-generated method stub
		return super.visit(node);
	}

	@Override
	public boolean visit(Modifier m) {
		if(m.isPublic())
		{
			String newSource = ASTOperators.changeAccessibility(m, ASTOperators.PRIVATE, cu, fileContent);
			saveNewVersion(newSource);
			newSource = ASTOperators.changeAccessibility(m, ASTOperators.PROTECTED, cu, fileContent);
			saveNewVersion(newSource);
			newSource = ASTOperators.changeAccessibility(m, ASTOperators.DEFAULT, cu, fileContent);
			saveNewVersion(newSource);
		}
		
		return super.visit(m);
	}

	@Override
	public boolean visit(TypeDeclaration node) {
		// TODO Auto-generated method stub
		return super.visit(node);
	}

	@Override
	public boolean visit(CatchClause node) {
		
		//node.setProperty("OP", ASTOprators.REMOVE);
		
		return super.visit(node);
	}
	
	private void saveNewVersion(String newSource)
	{
		UUID uuid = UUID.randomUUID();
		File dir = new File("C:\\Users\\Polaris\\Desktop\\Versions\\" + uuid);
		dir.mkdir();
		File file = new File("C:\\Users\\Polaris\\Desktop\\Versions\\" + uuid + "\\Management.java");
		try 
		{
			FileWriter writer = new FileWriter(file);
			writer.write(newSource);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
