package cs.utoronto.czhu.ast;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;

public class ASTOperators {
	public static final int ADD = 0;
	public static final int REMOVE = 1;
	public static final int UPDATE = 2;
	public static final int MOVE = 3;
	
	//Accessibility
	public static final int PRIVATE = 100;
	public static final int PROTECTED = 101;
	public static final int PUBLIC = 102;
	public static final int DEFAULT = 103;
	
	public static String changeAccessibility(Modifier m, int target, CompilationUnit cu, String fileContent)
	{
		Document document = new Document(fileContent);
		ASTRewrite rewrite = ASTRewrite.create(cu.getAST());
		
		Modifier newModifier = null;
		
		if(target == ASTOperators.PRIVATE)
		{
			newModifier = cu.getAST().newModifier(ModifierKeyword.PRIVATE_KEYWORD);
			rewrite.replace(m, newModifier, null);
		}
		else if(target == ASTOperators.PROTECTED)
		{
			newModifier = cu.getAST().newModifier(ModifierKeyword.PROTECTED_KEYWORD);
			rewrite.replace(m, newModifier, null);
		}
		else if(target == ASTOperators.PUBLIC)
		{
			newModifier = cu.getAST().newModifier(ModifierKeyword.PUBLIC_KEYWORD);
			rewrite.replace(m, newModifier, null);
		}
		else if(target == ASTOperators.DEFAULT)
		{
			rewrite.remove(m, null);
		}
		
		TextEdit edits = rewrite.rewriteAST(document, null);
		try {
			edits.apply(document);
		} catch (MalformedTreeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String newSource = document.get();
		return newSource;
	}
}
