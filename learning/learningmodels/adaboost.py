import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import AdaBoostClassifier
from sklearn import metrics
from sklearn import cross_validation
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectKBest

# load data
raw_data = open('./training_data', 'r')
raw_labels = open('./training_label', 'r')
dataset = np.loadtxt(raw_data, delimiter=",")
labelset = np.loadtxt(raw_labels, delimiter=",")

# exist_features = [0,2,4,5,6,7,8,9,10,11,13,16,17,20,25,26,27,28,29]
# X = dataset[:, exist_features]
X = dataset[:,:]
y = labelset

# =============================================
# data normalization
# =============================================

#==============================================
# feature selection
#==============================================
chi2 = chi2(X,y)
print '[CHI2]: '
print chi2

print '[KBESTSCORES]: ' 
# X = SelectKBest(k=10).fit_transform(X,y)
# print SelectKBest(k=10).fit(X,y).scores_

#==============================================
# training
#==============================================


ada = AdaBoostClassifier(n_estimators=100)
ada.fit(X,y)

# cross validation
scores = cross_validation.cross_val_score(ada, X, y, cv=5)
print '[SCORES]: '
print scores

predicted = cross_validation.cross_val_predict(ada, X, y, cv=5)
print '[PREDICTIONS]: '
print predicted

accuracy = metrics.accuracy_score(y, predicted)
print '[ACCURACY]: '
print accuracy

# predict & performance
#lin_svc.predict(validation_set)
#lin_svc.decision_function(validation_set)
#lin_svc.score(validation_set, valid_label_set)
#lin_svc.predict_proba(validation_set)
