benchmarks = open('./learning_datasets', 'r')
training_data = open('./training_data', 'a')
training_label = open('./training_label', 'a')
for line in benchmarks:
    if line.startswith('@'):
        print line + '\n'
        datafile = open(line.strip().replace('@',''), 'r')
        for l in datafile:
            if l.endswith(']\n'):
                dataline = l.split(',')
                sample = '['
                for item in dataline[-30:-1]:
                    sample = sample + item.strip() + ','
                #sample = sample[0:-1]
                #sample = sample + ']\n'
                sample = sample + dataline[len(dataline)-1].replace(' ', '')
                training_data.write(sample)
                training_label.write('0\n')
        training_data.write('==================================================================================\n')
        training_label.write('==================================================================================\n')
training_data.close()
training_label.close()
