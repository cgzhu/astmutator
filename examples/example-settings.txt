# You can pretend this file is -*- perl -*-

# Each configuration option corresponds to a variable in the Daikon source
# code.  Those variables have names identical to the configuration options,
# except that the variable names start with "dkconfig_" (for example,
# daikon.FileIO.dkconfig_add_changed).  That prefix makes them easy to find
# and reminds the reader that those variables should be set only via the
# configuration mechanism, never via direct assignment in Java code.

# All possible configuration options are described in the Daikon
# manual.  Command line switches to Daikon may specify a file (such as
# this one) that lists settings to read, or may specify single options
# directly.

# Here is an example of how one might change some settings.

# For invariants ...

 daikon.inv.unary.scalar.LowerBound.maximal_interesting = 1000000
 daikon.inv.unary.scalar.LowerBound.minimal_interesting = -1000
 daikon.inv.unary.scalar.RangeInt.Even.enabled = true
 daikon.inv.unary.scalar.LowerBoundFloat.maximal_interesting = 1000000
 daikon.inv.unary.scalar.LowerBoundFloat.minimal_interesting = -1000
 daikon.inv.unary.scalar.UpperBound.maximal_interesting = 1000000
 daikon.inv.unary.scalar.UpperBound.minimal_interesting = -1000
 daikon.inv.unary.scalar.UpperBoundFloat.maximal_interesting = 1000000
 daikon.inv.unary.scalar.UpperBoundFloat.minimal_interesting = -1000
 daikon.inv.unary.sequence.EltLowerBound.maximal_interesting = 1000000
 daikon.inv.unary.sequence.EltLowerBound.minimal_interesting = -1000
 daikon.inv.unary.sequence.EltLowerBoundFloat.maximal_interesting = 1000000
 daikon.inv.unary.sequence.EltLowerBoundFloat.minimal_interesting = -1000
 daikon.inv.unary.sequence.EltUpperBound.maximal_interesting = 1000000
 daikon.inv.unary.sequence.EltUpperBound.minimal_interesting = -1000
 daikon.inv.unary.sequence.EltUpperBoundFloat.maximal_interesting = 1000000
 daikon.inv.unary.sequence.EltUpperBoundFloat.minimal_interesting = -1000


daikon.inv.unary.scalar.OneOfScalar.size = 100


# For derived variables ...

daikon.inv.Invariant.confidence_limit = 0.0

# For splitters ...


