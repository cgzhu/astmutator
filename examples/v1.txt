===========================================================================
Management:::CLASS
===========================================================================
Management:::OBJECT
this has only one value
===========================================================================
Management.Management():::EXIT
===========================================================================
Management.calculateBasicSalary(int, int):::ENTER
arg0 == 12
arg1 one of { 0, 1, 2, 3 }
Management.WRONG < arg1
arg0 > arg1
===========================================================================
Management.calculateBasicSalary(int, int):::EXIT58
===========================================================================
Management.calculateBasicSalary(int, int):::EXIT
return one of { 60000, 96000, 120000, 360000 }
return != 0
Management.WRONG < return
return % orig(arg0) == 0
return > orig(arg0)
return > orig(arg1)
===========================================================================
Management.calculateSumSalary(int, int, int):::ENTER
arg0 == 12
arg1 one of { 0, 1, 2, 3 }
arg2 == 2000
Management.WRONG < arg1
arg0 > arg1
arg1 < arg2
===========================================================================
Management.calculateSumSalary(int, int, int):::EXIT
return one of { 124000, 192000, 240000, 724000 }
return != 0
Management.WRONG < return
return > orig(arg0)
return > orig(arg1)
return % orig(arg2) == 0
return > orig(arg2)
===========================================================================
Management.imagineAnIdealBonus(int, int):::ENTER
arg0 == 12
arg1 == 2000
===========================================================================
Management.imagineAnIdealBonus(int, int):::EXIT
return == 24000
===========================================================================
Management.saySomethingAboutBonus(int):::ENTER
arg0 == 2000
===========================================================================
Management.saySomethingAboutBonus(int):::EXIT
===========================================================================
Management.saySomethingAboutSalary(int):::ENTER
arg0 one of { 62000, 96000, 120000, 362000 }
arg0 != 0
Management.WRONG < arg0
===========================================================================
Management.saySomethingAboutSalary(int):::EXIT
===========================================================================
Positions:::CLASS
Positions.MANAGER == 0
Positions.EMPLOYEE == 1
Positions.DRIVER == 2
Positions.PROGRAMMER == 3
===========================================================================
Positions.getTitle(int):::ENTER
arg0 one of { 0, 1, 2, 3 }
Positions.MANAGER <= arg0
Positions.PROGRAMMER >= arg0
===========================================================================
Positions.getTitle(int):::EXIT12
Positions.MANAGER == orig(arg0)
return has only one value
return.toString == "Manager"
===========================================================================
Positions.getTitle(int):::EXIT16
Positions.EMPLOYEE == orig(arg0)
return has only one value
return.toString == "Employee"
===========================================================================
Positions.getTitle(int):::EXIT20
Positions.DRIVER == orig(arg0)
return has only one value
return.toString == "Driver"
===========================================================================
Positions.getTitle(int):::EXIT24
Positions.PROGRAMMER == orig(arg0)
return has only one value
return.toString == "Programmer"
===========================================================================
Positions.getTitle(int):::EXIT
Positions.MANAGER == orig(Positions.MANAGER)
Positions.EMPLOYEE == orig(Positions.EMPLOYEE)
Positions.DRIVER == orig(Positions.DRIVER)
Positions.PROGRAMMER == orig(Positions.PROGRAMMER)
return != null
Positions.MANAGER <= orig(arg0)
Positions.PROGRAMMER >= orig(arg0)